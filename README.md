# WordPress deploy scripts

## Description
This is a very simple group of scripts that I use to deploy WordPress when server doesn't have control panel or other means of automation.

## How to use
1. Clone the repo.
2. Run ***preparation.sh*** to install dependancies.
3. Run deploy script. It needs at least a ***domain name***, ***database name*** and ***database username***. Passwords will be randomly generated and printed in the end.
``` bash
./deploy_wp.sh --wpdomain=dev.radoslav-stefanov.me --dbuser=dev_radoslav_stefanov_me --database=dev_radoslav_stefanov_me_db
```

## Requirements
Ubuntu 18.04 LTS (or later)
