#!/bin/bash

WWW_PATH="/var/www"
VERSION="0.0.1a"

_bold=$(tput bold)
_underline=$(tput sgr 0 1)
_reset=$(tput sgr0)

_purple=$(tput setaf 171)
_red=$(tput setaf 1)
_green=$(tput setaf 76)
_tan=$(tput setaf 3)
_blue=$(tput setaf 38)

PACKAGES="nginx php-fpm mariadb-server htop curl php-mysql postfix python-certbot-nginx php-gd php-curl php-dom php-imagick php-mbstring apache2-utils webp redis php-redis"
LAST_UPDATED=$( stat --format="%X" /var/cache/apt/pkgcache.bin )
UNIX_TIME=$( date +%s )
TIME_DIFF=$(( UNIX_TIME - LAST_UPDATED ))
NGINX_BIN=$(which nginx)
BIN_MYSQL=$(which mysql)
