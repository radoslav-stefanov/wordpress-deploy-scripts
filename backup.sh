#!/bin/bash
############################### Variables ################################
WWW_PATH="/var/www"
DOMAINS="icanmanageit.com dev.icanmanageit.com"
SOURCE="$WWW_PATH/$DOMAIN/"
GC_BUCKET="my-wordpress-backups"
AUTHENTICATION="gc_storage_key.json"

# Google storage binaries paths.
GSUTIL="$(which gsutil)"
GCLOUD="$(which gcloud)"

. functions.sh

#set -x

[[ $1 != "run" ]] && _printBackupUsage

echo "$(date) Backup has started."
"$GCLOUD" auth activate-service-account --key-file="$AUTHENTICATION"
check_exit_status "Autenticate with Google Cloud platform..." FATAL

for domain in $DOMAINS; do
   SOURCE="$WWW_PATH/$domain/"
   DESTINATION="gs://$GC_BUCKET/$domain/"
  "$GSUTIL" -q -m rsync -d -c -C "$SOURCE" "$DESTINATION"
  check_exit_status "Push WordPress data for domain $domain to bucket..." FATAL
done

echo ""
echo "$(date)"
_success "Backup upload completed!"
