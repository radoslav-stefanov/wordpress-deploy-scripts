#!/bin/bash

#
# Script to manage WordPress installation
#
# @author   Radoslav Stefanov <rstefanov@icanmanageit.com>
# @website  https://icanmanageit.com
# @version  0.0.1a

. vars.sh
. functions.sh

export LC_CTYPE=C
export LANG=C

DB_HOST='localhost'
DB_NAME=
DB_USER=
DB_PASS=$(generate_db_password)
WP_PASS=$(generate_wp_password)
WP_ADMIN=

main "$@"
