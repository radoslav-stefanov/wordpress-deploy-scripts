#!/bin/bash

. vars.sh

function check_exit_status {
_rc=$?
  echo -n "$1 - ";
  if [ $_rc -eq 0 ]; then
    _success OK
  else
    if [ ". $2 ." = ". FATAL ." ]; then
      _die THIS_IS_A_FATAL_ERROR!!!
    fi
  fi
}

function _header() {
  printf "\n${_bold}${_purple}==========  %s  ==========${_reset}\n" "$@"
}

function _success() {
  printf "${_green}✔ %s${_reset}\n" "$@"
}

function _error() {
  printf "${_red}✖ %s${_reset}\n" "$@"
}

function _die() {
  _error "$@"
  exit 1
}

function _checkRootUser() {
  if [ "$(whoami)" != 'root' ]; then
      echo "You have no permission to run $0 as non-root user. Use sudo"
      exit 1;
  fi
}

function _printPoweredBy() {
  cat <<"EOF"

Powered By:
 ____           _           _
|  _ \ __ _  __| | ___  ___| | __ ___   __
| |_) / _` |/ _` |/ _ \/ __| |/ _` \ \ / /
|  _ < (_| | (_| | (_) \__ \ | (_| |\ V /
|_| \_\__,_|\__,_|\___/|___/_|\__,_| \_/
 ____  _        __
/ ___|| |_ ___ / _| __ _ _ __   _____   __
\___ \| __/ _ \ |_ / _` | '_ \ / _ \ \ / /
 ___) | ||  __/  _| (_| | | | | (_) \ V /
|____/ \__\___|_|  \__,_|_| |_|\___/ \_/


 >> Website: https://radoslav-stefanov.me

################################################################
EOF
}

function generate_db_password() {
  echo "$(openssl rand -base64 12)"
}

function generate_wp_password() {
  echo "$(openssl rand -base64 12)"
}

function _printUsage() {
  echo -n "$(basename $0) [OPTION]...

Provision WordPress with mySQL user and password.

Version $VERSION

    Options:
        --host        mySQL Host
        --database    mySQL Database
        --dbuser      mySQL User
        --dbpass      mySQL Password (If empty, auto-generated)
        --wpdomain    WordPress domain name
        --wpadmin     WordPress admin username
        --wppass      WordPress admin password
        --help        Display this help and exit
        --version     Output version information and exit

    Examples:
        $(basename $0) --help

"
  _printPoweredBy
  exit 1
}

function _printBackupUsage() {
  echo -n "
    Simple bash script to backup your WordPress website to Google Cloud Storage.

    To run manually just execute:  $(basename $0) run

"
  _printPoweredBy
  exit 1
}

function processArgs() {
  # Parse Arguments
  for arg in "$@"; do
    case $arg in
      --host=*)
        DB_HOST="${arg#*=}"
      ;;
      --database=*)
        DB_NAME="${arg#*=}"
      ;;
      --dbuser=*)
        DB_USER="${arg#*=}"
      ;;
      --dbpass=*)
        DB_PASS="${arg#*=}"
      ;;
      --wpdomain=*)
        WP_DOMAIN="${arg#*=}"
      ;;
      --wppass=*)
        WP_PASS="${arg#*=}"
      ;;
      --wpadmin=*)
        WP_ADMIN="${arg#*=}"
      ;;
      --help)
        _printUsage
      ;;
      *)
        _printUsage
      ;;
    esac
  done
  [[ -z $DB_NAME ]] && _error "WordPress database name cannot be empty." && exit 1
  [[ -z $WP_DOMAIN ]] && _error "WordPress domain cannot be empty." && exit 1
  [[ $DB_USER ]] || DB_USER=$DB_NAME
  [[ $WP_ADMIN ]] || WP_ADMIN=admin
}

function createmySQLDbUser() {
  SQL1="CREATE DATABASE IF NOT EXISTS ${DB_NAME};"
  SQL2="CREATE USER '${DB_USER}'@'%' IDENTIFIED BY '${DB_PASS}';"
  SQL3="GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO '${DB_USER}'@'%';"
  SQL4="FLUSH PRIVILEGES;"

  if [ -f /root/.my.cnf ]; then
    $BIN_MYSQL -e "${SQL1}${SQL2}${SQL3}${SQL4}"
  else
    $BIN_MYSQL -h $DB_HOST -u root -e "${SQL1}${SQL2}${SQL3}${SQL4}"
  fi
}

function printSuccessMessage() {
  echo ""
  _success "WordPress installation and configuration completed!"

  echo "################################################################"
  echo ""
  echo " >> Database host    : ${DB_HOST}"
  echo " >> Database name    : ${DB_NAME}"
  echo " >> Database user    : ${DB_USER}"
  echo " >> Database pwd     : ${DB_PASS}"
  echo " >> WordPress domain : ${WP_DOMAIN}"
  echo " >> WordPress pwd    : ${WP_PASS}"
  echo " >> WordPress admin  : ${WP_ADMIN}"
  echo ""
  echo "################################################################"
  #_printPoweredBy

}

function main() {
  [[ $# -lt 1 ]] && _printUsage
  processArgs "$@"

  check_wp_exists
  check_exit_status "Make sure $WWW_PATH/$WP_DOMAIN doesn't already exist..." FATAL

  create_website_dir
  check_exit_status "Create WordPress directory $WWW_PATH/$WP_DOMAIN..." FATAL

  enable_aux_configs
  check_exit_status "Enable aux configuration..." FATAL

  cd "$WWW_PATH/$WP_DOMAIN" 2>/dev/null 1>&2
  check_exit_status "Go to $WWW_PATH/$WP_DOMAIN directory..." FATAL

  create_vh
  check_exit_status "Prepare Nginx virtual host..." FATAL

  test_nginx_config
  check_exit_status "Test Nginx virtual host configuration file..." FATAL

  enable_vh
  check_exit_status "Enable Nginx virtual host..." FATAL

  reload_nginx
  check_exit_status "Apply Nginx configuration..." FATAL

  download_wp
  check_exit_status "Download latest WordPress..." FATAL

  fix_permissions
  check_exit_status "Fix permissions for WordPress files..." FATAL

  createmySQLDbUser
  check_exit_status "Create mySQL database and user..."

  configure_wp
  check_exit_status "Generate WordPress configuration wp-config.php file..." FATAL

  install_wp
  check_exit_status "Install WordPress..." FATAL

  deploy_plugins
  check_exit_status "Add WordPress plugins..." FATAL
  printSuccessMessage

  exit 0
}

function create_vh() {
  cat <<==end > /etc/nginx/sites-available/$WP_DOMAIN.conf
server {
    listen 80;
    server_name $WP_DOMAIN www.$WP_DOMAIN;

    root $WWW_PATH/$WP_DOMAIN;
    index index.php;

    location / {
      try_files \$uri \$uri/ /index.php?q=\$uri&\$args;
    }

    location ~*  \.(jpg|jpeg|webp|ttf|png|gif|ico|woff|woff2)$ {
      expires 365d;
    }

    location ~*  \.(css|js)$ {
      expires 365d;
    }

    location ~ \.php$ {
      fastcgi_split_path_info  ^(.+\.php)(/.+)$;
      #fastcgi_pass localhost:9001;
      fastcgi_pass unix:/run/php/php7.2-fpm.sock;
      fastcgi_index index.php;
      include fastcgi_params;
      fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }

    include speed.conf;
    include letsencrypt.conf;
}
==end
}

function enable_vh() {
  ln -sf /etc/nginx/sites-available/"$WP_DOMAIN".conf /etc/nginx/sites-enabled/"$WP_DOMAIN".conf
}

function reload_nginx() {
  /etc/init.d/nginx reload > /dev/null
}

function test_nginx_config() {
  $NGINX_BIN -t 2>/dev/null 1>&2
}

function create_website_dir() {
  mkdir "$WWW_PATH/$WP_DOMAIN"
}

function fix_permissions() {
  chown www-data:www-data -R "$WWW_PATH/$WP_DOMAIN"
}

function configure_wp() {
  sudo -u www-data wp core config --dbhost=$DB_HOST --dbname=$DB_NAME --dbuser=$DB_USER --dbpass=$DB_PASS > /dev/null
}

function download_wp() {
  wp core download --allow-root > /dev/null
}

function install_wp() {
  sudo -u www-data wp core install --url="$WP_DOMAIN" --title="Blog Title" --admin_user="$WP_ADMIN" --admin_password="$WP_PASS" --admin_email="email@domain.com" > /dev/null
}

function deploy_plugins() {
  sudo -u www-data wp plugin install WebP Express --activate > /dev/null
}

function enable_aux_configs() {
  CONFIGS="speed.conf letsencrypt.conf"
  for c in $CONFIGS; do
    cp files/"$c" /etc/nginx/
  done
}

function check_wp_exists() {
  if test -d "$WWW_PATH/$WP_DOMAIN"; then
    return 1
  fi
}
