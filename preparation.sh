#!/bin/bash

. functions.sh
. vars.sh

# Update
if [[ "${TIME_DIFF}" -gt 43200 ]]
then
  _header Update_apt_sources
  echo "APT cache older than 12 hours, going to try update."
  apt update -qq
  check_exit_status "Update apt sources."
fi

# Upgrade
CHECK_UPGRADABLE=$( apt-get -s dist-upgrade | grep -Po "^[[:digit:]]+")
if [[ "${CHECK_UPGRADABLE}" -gt 0 ]]
then
  _header Apply_all_available_updates
  echo "Total ${CHECK_UPGRADABLE} of packages can be upgraded. Doing so now."
  export DEBIAN_FRONTEND=noninteractive && apt-get --with-new-pkgs -y -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" upgrade > /dev/null
  check_exit_status "Apply all available updates."
fi

# Enable Let's Encrypt repository, so we always get latest certbot version
if [ ! -f /etc/apt/sources.list.d/certbot-ubuntu-certbot-bionic.list ]; then
  export DEBIAN_FRONTEND=noninteractive && apt-get install software-properties-common -y < /dev/null > /dev/null
  check_exit_status "Install software-properties-common package..."
  export DEBIAN_FRONTEND=noninteractive && add-apt-repository ppa:certbot/certbot -y < /dev/null > /dev/null
  check_exit_status "Add certbot repository..."
fi

# Install packages
_header Check_for_required_packages
for p in ${PACKAGES}; do
  apt-get install -o Dpkg::Options::=--force-confnew "${p}" --assume-yes < /dev/null > /dev/null
  check_exit_status "$p"
done

# Install WordPress command line interface
if [ ! -f /usr/local/bin/wp ]; then
  curl -sSO https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar > /dev/null
  check_exit_status "Download WP-CLI..."

  chmod +x wp-cli.phar
  check_exit_status "Add executable flag..."

  mv wp-cli.phar /usr/local/bin/wp
  check_exit_status "Move wp-cli.phar to /usr/local/bin/wp"
fi
